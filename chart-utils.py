#!/usr/bin/env python3

import yaml
import datetime
import logging
import os
import subprocess
from pathlib import Path
import re

# Logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

logger = logging.getLogger(__name__)


class helmtools:
    def __init__(self, **kwargs):
        self.kwargs = kwargs
        self.kwargs["datetime"] = datetime.datetime.now().isoformat()
        self.kwargs["images"] = ["nginx:alpine"]
        self.kwargs["charts"] = ["bitnami/nginx"]

    def scrape_yaml_for_images(self, yaml):
        with open("minio/values.yaml") as f:
            data = yaml.load(f, Loader=yaml.FullLoader)
            print(data)
        return self.kwargs["images"]

    def docker_pull_images(self, images):
        if images:
            self.kwargs["images"] = images
        pass

    def list_helm_charts(self):
        list_helm_charts_subprocess = subprocess.Popen(
            [
                "microk8s",
                "helm3",
                "repo",
                "add",
                "bitnami",
                "https://charts.bitnami.com/bitnami",
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        for line in list_helm_charts_subprocess.stdout:
            print(line)

        list_helm_charts_subprocess = subprocess.Popen(
            ["microk8s", "helm3", "search", "repo", "bitnami"],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )
        for line in list_helm_charts_subprocess.stdout:
            chart_list = []
            # print(line.decode("utf8", errors="strict").strip().split("\t"))
            print(line.decode("utf8", errors="strict").strip())
            print(re.split("[\t,]", line.decode("utf8", errors="strict").strip()))
            chart_list.append("1 ")
            self.kwargs["charts"] = chart_list

        if list_helm_charts_subprocess.returncode != 0:
            return 1
        else:
            return 0

        # microk8s helm3 search repo bitnami  | sed -E "/DEPRECATED/d;/NAME/d" | awk '{print $1}' | xargs -I {} helm3 pull {}
        pass

    def get_helm_chart():
        pass


helm_job = helmtools()

helm_job.list_helm_charts()
