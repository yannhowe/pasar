## Setup

Setup microk8s 
```
microk8s enable storage metrics-server metallb helm3 dns
echo 'alias kubectl="microk8s kubectl"' >> ~/.bash_aliases
echo 'alias helm="microk8s helm3"' >> ~/.bash_aliases
```

Setup helm
```
helm repo add bitnami https://charts.bitnami.com/bitnami
```

## Install Helm Chart

### Online
```
kubectl create namespace helm
helm install my-release bitnami/nginx -n helm
helm list
```

### Offline
```
kubectl create namespace helm
helm pull bitnami/nginx # online
helm install my-offline-release nginx-*.tgz -n helm
helm get all my-offline-release -n
echo "http://$(kubectl get svc --namespace default my-offline-release-nginx -o jsonpath='{.status.loadBalancer.ingress[0].ip}'):$(kubectl get --namespace default -o jsonpath="{.spec.ports[0].port}" services my-offline-release-nginx)"
```

# Scraper